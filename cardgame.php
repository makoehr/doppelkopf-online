<?php
// Cardgame Engine

include "config.php";

session_cache_expire(120); 
session_start();


$DATABASE_BASENAME = "./database";

class CardDatabase extends SQLite3
{

    private function createTables()
    {
        $this->exec("BEGIN TRANSACTION");
        $this->exec('CREATE TABLE carddeck (id INTEGER PRIMARY KEY UNIQUE, name TEXT, value INTEGER)');
        $this->exec("CREATE TABLE player   (id INTEGER PRIMARY KEY, name TEXT UNIQUE, generatedName INTEGER DEFAULT 0, activeTime TEXT DEFAULT (datetime('now')))");
        $this->exec(
            'CREATE TABLE cards (' .
                'cardid INTEGER UNIQUE,' .        # The id of the card
                'playerid INTEGER,' .             # The id of the player that owns the card
                'sortorder INTEGER DEFAULT 0,' .  # The sortorter of the player
                'stackid INTEGER,' .              # The player that holds the card in the stack
                'openid INTEGER,' .               # The card is visible and belongs to this player
                'playoutno INTEGER DEFAULT 0,' .  # The order in which the cards have been played out
                'originid INTEGER,' .             # The origin owner of the card before taking it
                'move INTEGER DEFAULT 0,' .       # The move number
                'exchangeid INTEGER' .            # The card is in exchange number 'exchangeid'
             ')');
        $this->exec('CREATE TABLE count    (playerid INTEGER PRIMARY KEY UNIQUE, count INTEGER)');
        $this->exec('CREATE TABLE info     (key TEXT PRIMARY KEY, val TEXT)');

        $this->exec("INSERT INTO info (key) VALUES ('nextplayer')");

        $this->exec("INSERT INTO info (key) VALUES ('shuffleOptionsJson')");

        $this->exec("INSERT INTO info (key, val) VALUES ('shuffleCount', 0)");


        $this->exec("COMMIT TRANSACTION");
    }

    public function openDatabase($database)
    {
        $dbAvailable = is_readable($database);
        if ($dbAvailable)
        {
            // Re-Generate old databases
            if (time() - filemtime($database) > 30*60)
            {
                error_log("Delete old database $database");
                unlink($database);
                $dbAvailable = false;
            }
        }
        $this->open($database);
        if (! $dbAvailable)
        {
            $this->createTables();
        }
        return $dbAvailable;
    }

    public function __construct()
    {
    }
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

function mybacktrace()
{
    ob_start();
    #debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    debug_print_backtrace();
    error_log(ob_get_contents());
    ob_end_clean();
}


abstract class CardGame
{

    public $db;
    public $NUMBER_PLAYERS = 0;
    public $table;
    public $nameOfTheGame;  // Full name as displayed e.g. in the login title
    public $shortName;  // only lowercase, no spaces

    public function __construct()
    {
    }

    protected function printPlayHtml()
    {
        readfile("{$this->shortName}.html");
    }

    protected function getDatabaseName()
    {
        global $DATABASE_BASENAME;
        $database = "{$DATABASE_BASENAME}-{$this->shortName}-{$this->table}.db";
        return $database;
    }

    private function doDeleteDB($table)
    {
        unlink($this->getDatabaseName());
    }

    private function getQmarkList($cnt)
    {
        return str_repeat('?,', $cnt -1) . '?';
    }


    abstract protected function shuffleCards($player, $shuffleOptionsJson);

    private function doShuffleCards($player, $shuffleOptionsJson)
    {
        $this->db->exec("BEGIN TRANSACTION");
        $this->shuffleCards($player, $shuffleOptionsJson);


        $this->setShuffleOptions($shuffleOptionsJson);
        $this->db->exec("UPDATE info SET val=val+1 WHERE key = 'shuffleCount'");
        $this->db->exec("END TRANSACTION");
    }

    // Set shuffle options (as JSON!) to be stored in the database.
    protected function setShuffleOptions($shuffleOptionsJson)
    {
        $stmt = $this->db->prepare("UPDATE info SET val=? WHERE key = 'shuffleOptionsJson'");
        $stmt->bindValue(1, $shuffleOptionsJson);
        $stmt->execute();
    }

    private function getShuffleOptions()
    {
        $results = $this->db->query("SELECT val FROM info WHERE key = 'shuffleOptionsJson'");
        if ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $val = $row['val'];
            if ($val)
            {
                echo $val;
                return;
            }
        }
        echo "{}";
    }

    // If players are inactive, set their names to null
    protected function checkInactivePlayers()
    {
        $this->db->exec("DELETE FROM player WHERE generatedName = 0 AND activeTime <= datetime('now', '-30 seconds')");
    }

    protected function setNextPlayer($p)
    {
        $stmt = $this->db->prepare("UPDATE info SET val=? WHERE key = 'nextplayer'");
        $stmt->bindValue(1,$p);
        $stmt->execute();
    }


    private function getPlayerName($player)
    {
        $stmt = $this->db->prepare("SELECT name FROM player WHERE id = ?");
        $stmt->bindValue(1,$player);
        $results = $stmt->execute();
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            return $row['name'];
        }
        return null;
    }

    private function getPlayerId($name)
    {
        $stmt = $this->db->prepare("SELECT id FROM player WHERE name = ?");
        $stmt->bindValue(1,$name);
        $results = $stmt->execute();
        if ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            return $row['id'];
        }
        return null;
    }

    private function doInit($table, $name, $admin)
    {

        $result["player"] = $this->getPlayerId($name);
        $result["playerName"] = $name;
        $result["table"] = $table;
        $result["admin"] = $admin;


        // Check for the session variable of cardHeight
        if (! isset($_SESSION['cardHeight']))
        {
            $_SESSION['cardHeight'] = 180;
        }
        $result["cardHeight"] = $_SESSION['cardHeight'];

        echo json_encode($result);
    }

    private function getPlayersInfo()
    {
        $playerList = array();
        $posList = array();
        $results = $this->db->query("SELECT id FROM player ORDER BY id ASC");
        $pos = 0;
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $id = $row['id'];
            $playerList[$pos] = $id;
            $posList[$id] = $pos;
            $pos++;
        }
        return [ "players" => $playerList, 
                 "positions" => $posList ];
    }

    protected function getRelativePlayer($player, $offset)
    {
        $info = $this->getPlayersInfo();

        $playerPos = $info["positions"][$player];
        $next = ($playerPos + $offset) % $self->NUMBER_PLAYERS;
        $nextPlayer = $info["players"][$next];

        return $nextPlayer;

    }


    private function getAllPlayers()
    {
        return $this->getPlayersInfo()["players"];
    }


    private function getAbsolutePlayerPositions()
    {
        return $this->getPlayersInfo()["positions"];
    }

    protected function relativePositionOfPlayer($player, $me)
    {
        $absPosition = $this->getAbsolutePlayerPositions();
        $myPosition = $absPosition[$me];
        $playerPosition = $absPosition[$player];
        $rel = ($this->NUMBER_PLAYERS + $playerPosition - $myPosition) % $this->NUMBER_PLAYERS;
        return $rel;
    }

    private function getCardsOfPlayer($player)
    {
        $this->checkInactivePlayers();

        $cardDeck = array();
        $results = $this->db->query("SELECT name FROM carddeck ORDER BY id");
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $cardDeck[] = $row['name'];
        }
        $result['cardDeck'] = $cardDeck;

        $cards = array();

        $stmt = $this->db->prepare("SELECT cardid FROM cards WHERE playerid = ? ORDER BY sortorder ASC, cardid ASC");
        $stmt->bindValue(1,$player);
        $results = $stmt->execute();
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $cards[] = $row['cardid'];
        }
        $result["cards"] = $cards;

        $playerInfo = array();
        $results = $this->db->query("SELECT id, name FROM player");
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $id = $row['id'];
            $name = $row['name'];
            $playerInfo[] = [ "pos" => $this->relativePositionOfPlayer($id, $player),
                              "name" => $name ];
        }
        $result["player"] = $playerInfo;


        echo json_encode($result);
    }



    private function playCards($cardids)
    {
        $qmarks = $this->getQmarkList(count($cardids));

        $this->db->exec("BEGIN TRANSACTION");
        $stmt = $this->db->prepare(
            "UPDATE cards
             SET openid=playerid,
                 playerid=NULL,
                 playoutno=(SELECT max(playoutno)+1 FROM cards)
             WHERE cardid IN ($qmarks)"
        );
        $bindpos = 1;
        foreach ($cardids as $c)
        {
            $stmt->bindValue($bindpos, $c);
            $bindpos++;
        }
        $stmt->execute();
        $this->db->exec("COMMIT TRANSACTION");
    }

    private function unplayCards($player)
    {
        $this->db->exec("BEGIN TRANSACTION");
        $stmt = $this->db->prepare("SELECT cardid FROM cards WHERE openid = ?");
        $stmt->bindValue(1,$player);
        $results = $stmt->execute();
        $cards = array();
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $cards[] = $row["cardid"];
        }
        $stmt = $this->db->prepare("UPDATE cards SET playerid=openid, openid=NULL, playoutno=0 WHERE openid = ?");
        $stmt->bindValue(1,$player);
        $stmt->execute();
        $this->db->exec("COMMIT TRANSACTION");

        echo json_encode($cards);
    }


    private function periodicInfo($player)
    {
        $opencards = array();
        $this->db->exec("BEGIN TRANSACTION");
        $stmt = $this->db->prepare("SELECT cardid, playoutno FROM cards WHERE openid = ? ORDER BY playoutno");
        $stmtNumberCards = $this->db->prepare("SELECT COUNT(stackid) as noCards FROM cards WHERE stackid = ?");
        $allPlayers = $this->getPlayersInfo()["players"];
        foreach ($allPlayers as $checkplayer)
        {
            $stmt->bindValue(1,$checkplayer);
            $results = $stmt->execute();
            $stmt->reset();
            $cards = array();
            while ($row = $results->fetchArray(SQLITE3_ASSOC))
            {
                $cards[] = [
                    "cardid" => $row["cardid"],
                    "playoutno" => $row["playoutno"]
                ];
            }

            $stmtNumberCards->bindValue(1, $checkplayer);
            $results = $stmtNumberCards->execute();
            $stmtNumberCards->reset();
            $noCards = 0;
            if ($row = $results->fetchArray(SQLITE3_ASSOC))
            {
                $noCards = $row["noCards"];
            }

            $opencards[] = [ "pos" => $this->relativePositionOfPlayer($checkplayer, $player),
                             "cards" =>  $cards,
                             "noCards" => $noCards,
            ];
        }
        $answer['openCards'] = $opencards;

        $info = array();
        $val = $this->db->querySingle("SELECT val FROM info WHERE key = 'shuffleCount'");
        if (!$val)
        {
            $val = "0";
        }
        $info['shuffleCount'] = $val;

        $now = time();
        $activeTime = 0;
        if (isset($_SESSION['activeTime']))
        {
            $activeTime = $_SESSION['activeTime'];
        }

        if ($now - $activeTime >= 10)
        {
            // Update timestamp of player activity every couple of seconds only...
            $stmt = $this->db->prepare("UPDATE player SET activeTime=datetime('now') WHERE id=?");
            $stmt->bindValue(1,$player);
            $stmt->execute();
            $_SESSION['activeTime'] = $now;
        }



        $results = $this->db->query("SELECT val FROM info WHERE key = 'nextplayer'");
        $this->db->exec("COMMIT TRANSACTION");
        if ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $val = $row['val'];
            if (! is_null($val))
            {
                $info['nextplayerId'] = $val;
                $info['nextplayerPos'] = $this->relativePositionOfPlayer($val, $player);
            }
        }

        $maxPlayoutorder = 0;
        $results = $this->db->query("SELECT MAX(playoutno) AS maxPlayoutno FROM cards");
        if ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $maxPlayoutorder = $row["maxPlayoutno"];
        }
        $answer['maxPlayoutno'] = $maxPlayoutorder;


        $info['player'] = $player;
        $answer['info'] = $info;


        echo json_encode($answer);

    }

    private function reorderCards($orderList)
    {
        $this->db->exec("BEGIN TRANSACTION");
        $stmt = $this->db->prepare("UPDATE cards SET sortorder=? WHERE cardid = ?");
        foreach ($orderList as $o)
        {
            $cardid = $o['cardid'];
            $order = $o['order'];
            $stmt->bindValue(1,$order);
            $stmt->bindValue(2,$cardid);
            $stmt->execute();
            $stmt->reset();
        }
        $this->db->exec("END TRANSACTION");
    }

    private function pushOnStack($player)
    {
        $this->db->exec("BEGIN TRANSACTION");

        $stmt = $this->db->prepare("UPDATE cards SET " .
                      "stackid=? ,".
                      "originid=openid,".
                      "openid=NULL,".
                      "move=(SELECT max(move)+1 FROM cards)".
                      "WHERE openid NOT NULL");
        $stmt->bindValue(1,$player);
        $stmt->execute();
        $changes = $this->db->changes();
        if ($changes > 0)
        {
            $stmt = $this->db->prepare("UPDATE info SET val=? WHERE key = 'nextplayer'");
            $stmt->bindValue(1,$player);
            $stmt->execute();

            $val = $this->db->querySingle("SELECT COUNT(*)=0 FROM cards WHERE stackid IS NULL");
            $allPlayers = $this->getAllPlayers();
            if ($val)
            {
                # All cards have been played out.
                $this->db->exec("DELETE FROM count");

                foreach ($allPlayers as $pl)
                {
                    $results = $this->db->exec("INSERT INTO count (playerid,count) VALUES " . 
                        "($pl, ".
                        "(SELECT COALESCE(sum(carddeck.value),0)  " .
                              "FROM cards,carddeck " .
                              "WHERE $pl = cards.stackid " .
                              "AND cards.cardid = carddeck.id) ) ");
                }
            }
            else
            {
                $this->db->exec("DELETE FROM count");
                foreach ($allPlayers as $pl)
                {
                    $results = $this->db->exec("INSERT INTO count (playerid,count) VALUES ($pl, 0)");
                }
            }
        }
        $this->db->exec("COMMIT TRANSACTION");
        return $changes;
    }

    private function popFromStack()
    {
        $this->db->exec("BEGIN TRANSACTION");
        $this->db->exec("UPDATE cards SET playerid=openid, openid=NULL WHERE openid NOT NULL");
        $this->db->exec("UPDATE cards SET ".
                      "openid=originid,".
                      "stackid=NULL,".
                      "originid=NULL,".
                      "move=0 ".
                  "WHERE move = (SELECT max(move) FROM cards)");
        $this->db->exec("COMMIT TRANSACTION");
    }

    private function exchangeWrite($cards, $exchangeId)
    {
        $cardlist = implode(",", $cards);
        $qmarks = $this->getQmarkList(count($cards));

        $this->db->exec("BEGIN TRANSACTION");
        $stmt = $this->db->prepare("UPDATE cards SET exchangeid=?, playerid=NULL WHERE cardid IN ($qmarks)");

        $stmt->bindValue(1,$exchangeId);
        $bindpos = 2;
        foreach ($cards as $c)
        {
            $stmt->bindValue($bindpos, $c);
            $bindpos++;
        }
        $result = $stmt->execute();
        $this->db->exec("COMMIT TRANSACTION");
    }

    private function exchangeRead($player, $exchangeId)
    {
        $this->db->exec("BEGIN TRANSACTION");
        $cards = array();
        $stmt = $this->db->prepare("SELECT cardid FROM cards WHERE exchangeid=?");
        $stmt->bindValue(1,$exchangeId);
        $results = $stmt->execute();
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $cards[] = $row['cardid'];
        }
        $stmt = $this->db->prepare("UPDATE cards SET playerid=?, exchangeid=NULL WHERE exchangeid=?");
        $stmt->bindValue(1,$player);
        $stmt->bindValue(2,$exchangeId);
        $stmt->execute();
        $this->db->exec("COMMIT TRANSACTION");
        echo json_encode($cards);
    }


    private function countCards()
    {

        $result = array();
        $results = $this->db->query("SELECT player.name AS name, count.count AS sum " .
                              "FROM player,count " .
                              "WHERE player.id = count.playerid" );
        while ($row = $results->fetchArray(SQLITE3_ASSOC))
        {
            $result[] = $row;
        }
        $data = array();
        $data['result'] = $result;
        echo json_encode($data);
    }

    private function setCardHeight($arg)
    {
        if ($arg >= 40 and $arg <= 500)
        {
            $_SESSION['cardHeight'] = $arg;
        }
        $data = array();
        $data['cardHeight'] = $_SESSION['cardHeight'];
        echo json_encode($data);
    }

    private function doLogout($player)
    {
        $stmt =$this->db->prepare("DELETE FROM player WHERE id=?");
        $stmt->bindValue(1,$player);
        $stmt->execute();

        unset($_SESSION['player']);
        unset($_SESSION['name']);
        unset($_SESSION['table']);
    }
    /////////////////////////////////////////////////////////////////////////////////
    private function getPOST($key)
    {
        if (isset($_POST[$key]))
        {
            return $_POST[$key];
        }
        return null;
    }

    private function getREQUEST($key)
    {
        if (isset($_REQUEST[$key]))
        {
            return $_REQUEST[$key];
        }
        return null;
    }

    private function printLoginHtml()
    {

        global $NUMBER_TABLES;
        global $ACCESS_CODE;

        $tableSelect = "";
        for ($i=1; $i<= $NUMBER_TABLES; $i++)
        {
            $tableSelect .= "<option value=\"$i\">Tisch $i</option>";
        }
        $code = "Admincode";
        if ($ACCESS_CODE != "")
        {
            $code = "Zugangscode";
        }

        $nameOfTheGame = $this->nameOfTheGame;

        echo <<<EOT
<!DOCTYPE html>
<html>
<head>
<title>$nameOfTheGame Anmeldung</title>
<!--<link rel="stylesheet" type="text/css" href="style.css">-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<link rel="stylesheet" href="jquery-ui/jquery-.css">
<link rel="stylesheet" href="doppelkopf.css">
<style>
</style>
<script src="jquery.js"></script>
<script src="jquery-ui.js"></script>
<script>
$(document).ready( function() {

    $("button").addClass("ui-button ui-widget compact-button");

    $("#login").click(function() {
        var name=$("#name").val();
        var password=$("#password").val();
        var table=$("#table option:selected").val();
        $.ajax({
            method: "POST",
            data: { c: "login", password: password, name: name, table:table  }
        })
         .done(function(json) {
             var obj = jQuery.parseJSON(json);
             if (obj.result == 0)
             {
                 // Success
                 location.reload();
             }
             else
             {
                 $("#error").text(obj.text);
             }
        });
    });

});

</script>
</head>
<body>
<h1>Anmeldung</h1>
<form onsubmit="return false;">
<fieldset>
    <div>
        <label for="name">Name</label>
        <input type="text" id="name">
    </div>
    <div>
        <label for="table">Tisch</label>
        <select id="table">$tableSelect</select>
    </div>
    <div>
        <label for="password">$code</label>
        <input type="password" id="password">
    </div>
    <div>
        <button type="submit" id="login">Tisch beitreten</button>
    </div>
    <div id="error"></div>
</fieldset>
</form>
</body>
</html>
EOT;

    }


    private function sendError($text)
    {
        $result['result'] = 1;
        $result['text'] = $text;
        echo json_encode($result);
        return false;
    }

    private function sendOK()
    {
        $result['result'] = 0;
        $result['text'] = "OK";
        echo json_encode($result);
        return true;
    }

    protected function createDatabase()
    {
        $this->db = new CardDatabase();
        $wasAvailable = $this->db->openDatabase($this->getDatabaseName());

        $this->db->exec("BEGIN TRANSACTION");
        if (! $wasAvailable)
        {
            $this->onCreateDatabaseNew();
        }
        $this->onCreateDatabase();
        $this->db->exec("COMMIT TRANSACTION");
    }

    /** Might be overwritten. Will be called on the end of the creation of the database.
     * The call is within a transaction.
     * */
    protected function onCreateDatabase()
    {
    }

    /** Might be overwritten. Will be called on the creation of a new database.
     * The call is within a transaction.
     * */
    protected function onCreateDatabaseNew()
    {
    }

    private function doLogin()
    {
        $password = $this->getPOST('password');
        $name = $this->getPOST('name');
        $table = $this->getPOST('table');

        $result = array();
        if (!$name)
        {
            return $this->sendError("Fehlender Name");
        }
        global $ACCESS_CODE;
        if ($name == "admin" || $ACCESS_CODE)
        {
            if(!$password)
            {
                 return $this->sendError("Fehlender Code");
            }
        }

        global $NUMBER_TABLES;
        if ($table <= 0 or $table > $NUMBER_TABLES)
        {
            return $this->sendError("Tisch ungültig");
        }

        if ($name == "admin")
        {
            global $ADMIN_PASSWORD;
            if ($password != $ADMIN_PASSWORD)
            {
                return $this->sendError("Falscher admin Zugangscode");
            }

            $_SESSION['name'] = $name;
            $_SESSION['player'] = 0;
            $_SESSION['table'] = $table;
            $_SESSION['admin'] = 1;

            return $this->sendOK();
        }

        if ($password != $ACCESS_CODE)
        {
            return $this->sendError("Falscher Code");
        }

        $this->table = $table;

        $this->createDatabase();

        $this->checkInactivePlayers();

        $id = -1;
        $this->db->exec("BEGIN TRANSACTION");
        $this->db->exec("DELETE FROM player WHERE generatedName = 1");
        $number = $this->db->querySingle("SELECT COUNT(*) FROM player");
        if ($number < 4)
        {
            $stmt = $this->db->prepare("INSERT INTO player (name) VALUES (?)");
            $stmt->bindValue(1,$name);
            if ($stmt->execute())
            {
                $stmt = $this->db->prepare("SELECT id FROM player where name = ?");
                $stmt->bindValue(1,$name);
                $results = $stmt->execute();
                if ($row = $results->fetchArray(SQLITE3_ASSOC))
                {
                    $id = $row['id'];
                }
            }
            $number++;
            while ($number < 4)
            {
                $this->db->exec("INSERT INTO player (name, activeTime, generatedName) VALUES ('Player{$number}', datetime('now'), 1)");
                $number++;
            }
        }
        $this->db->exec("COMMIT TRANSACTION");

        if ($id >= 0)
        {
            $_SESSION['name'] = $name;
            $_SESSION['player'] = $id;
            $_SESSION['table'] = $table;
            $_SESSION['admin'] = 0;
        }
        else
        {
            return $this->sendError("Zu viele Mitspieler oder nicht eindeutiger Name");
        }

        return $this->sendOK();
    }



    /// main

    public function main()
    {
        $cmd = $this->getREQUEST('c');

        if ( ! isset($_SESSION['player']) or (!isset($_SESSION['table'])))
        {
            unset($_SESSION['table']);
            unset($_SESSION['player']);
            unset($_SESSION['admin']);
            unset($_SESSION['name']);
            switch ($cmd)
            {
            case 'login':
                $this->doLogin();
                break;
            default:
                $this->printLoginHtml();
            }
            return;
        }

        $admin = $_SESSION['admin'];
        $table = $_SESSION['table'];
        $this->table = $table;

        $this->createDatabase();
        $player = $_SESSION['player'];
        $name = $_SESSION['name'];

        switch ($cmd)
        {
        case 'periodicInfo':
            $this->periodicInfo($player);
            break;

        case 'getCards':
            $this->getCardsOfPlayer($player);
            break;

        case 'playCards':
            $cards = $this->getREQUEST('cards');
            $this->playCards($cards);
            break;

        case 'shuffle':
            $shuffleOptionsJson = $this->getREQUEST('shuffleOptions');
            $this->doShuffleCards($player, $shuffleOptionsJson);
            break;

        case 'getShuffleOptions':
            $this->getShuffleOptions();
            break;

        case 'reorder':
            $order = $this->getREQUEST('order');
            $this->reorderCards($order);
            break;

        case 'exchangeWrite':
            $cards = $this->getREQUEST('cards');
            $exchangeId = $this->getREQUEST('exchangeId');
            $this->exchangeWrite($cards, $exchangeId);
            break;

        case 'exchangeRead':
            $exchangeId = $this->getREQUEST('exchangeId');
            $this->exchangeRead($player, $exchangeId);
            break;

        case 'countCards':
            $this->countCards();
            break;

        case 'pushOnStack':
            $this->pushOnStack($player);
            break;

        case 'popFromStack':
            $this->popFromStack();
            break;

        case 'unplayCards':
            $this->unplayCards($player);
            break;

        case 'setCardHeight':
            $arg = $this->getREQUEST('arg');
            $this->setCardHeight($arg);
            break;

        case 'init':
            $this->doInit($table, $name, $admin);
            break;

        case 'setPlayer':
            if ($admin)
            {
                $name = $this->getREQUEST('name');
                $player = $this->getPlayerId($name);
                $_SESSION['player'] = $player;
                $_SESSION['name'] = $name;
            }
            break;

        case 'logout':
            $this->doLogout($player);
            break;

        case 'deleteDB':
            if ($admin)
            {
                $this->doDeleteDB($table);
            }
            break;

        default:
            $this->printPlayHtml();
            break;
        }
    }
}

?>
