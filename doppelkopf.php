<?php


include "cardgame.php";



class DoppelkopfGame extends CardGame
{
    public function __construct()
    {
        parent::__construct();
        $this->NUMBER_PLAYERS = 4;
        $this->nameOfTheGame = "Doppelkopf";
        $this->shortName = "doppelkopf";
    }

    protected function onCreateDatabaseNew()
    {
        // The data base has just been created - do some initial steps

        // Set default shuffle option
        $this->setShuffleOptions('{"use9":true}'); // JSON!

    }

    protected function shuffleCards($player, $shuffleOptionsJson)
    {
        $options = json_decode($shuffleOptionsJson, true);
        $use9 = true;
        if (isset($options['use9']))
        {
            $use9 = $options['use9'];
        }

        $list = [
            ["Herz",  "10", 10],
            ["Kreuz", "D", 3],
            ["Pik",   "D", 3],
            ["Herz",  "D", 3],
            ["Karo",  "D", 3],
            ["Kreuz", "B", 2],
            ["Pik",   "B", 2],
            ["Herz",  "B", 2],
            ["Karo",  "B", 2],
            ["Karo",  "As", 11],
            ["Karo",  "10", 10],
            ["Karo",  "K", 4],
            ["Karo",  "9", 0],
            ["Kreuz", "As", 11],
            ["Kreuz", "10", 10],
            ["Kreuz", "K", 4],
            ["Kreuz", "9", 0],
            ["Herz",  "As", 11],
            ["Herz",  "K", 4],
            ["Herz",  "9", 0],
            ["Pik",   "As", 11],
            ["Pik",   "10", 10],
            ["Pik",   "K", 4],
            ["Pik",   "9", 0]
        ];

        $this->db->exec("DELETE from carddeck");
        $stmt = $this->db->prepare("INSERT INTO carddeck (id,name,value) VALUES (?, ?, ?)");

        $id = 0;
        foreach ($list as $card)
        {
            $color = $card[0];
            $text = $card[1];
            $value = $card[2];
            if (($text != "9") or $use9)
            {
                for ($i=0; $i<2; $i++)
                {
                    $stmt->bindValue(1,$id);
                    $stmt->bindValue(2,"$color-$text");
                    $stmt->bindValue(3,$value);
                    $stmt->execute();
                    $stmt->reset();
                    $id++;
                }
            }
        }
        $numCards = $id;



        $arr = array();
        for ($i=0; $i < $numCards; $i++)
        {
            $arr[] = $i;
        }
        shuffle($arr);
        $this->db->exec("DELETE FROM cards");
        $stmt = $this->db->prepare("INSERT INTO cards (cardid, playerid) VALUES (?,?)");
        for ($i=0; $i < $numCards; $i++)
        {
            $p = ($i % $this->NUMBER_PLAYERS);
            $card = $arr[$i];
            $stmt->bindValue(1,$card);
            $stmt->bindValue(2,$p);
            $stmt->execute();
            $stmt->reset();
        }


        $p = $this->relativePositionOfPlayer($player,1);
        $this->setNextPlayer($p);


    }


};

$game = new DoppelkopfGame();
$game->main();

?>
