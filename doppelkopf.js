
var cardDeck;

var cardHeight;

var shuffleCount;

var intervalHandle;

function createCardEntry(element)
{
    var imgsrc = "deck/" + cardDeck[element] + ".svg";
    var it = $('<li class="card-list-element"><img src="' + imgsrc + '"></li>');
    it.data("cardid", element); // Save cardid for card within the list element

    // Load image for the case that it is not in the cache:
    var img = it.find("img");
    var downloadImg = $("<img>");
    downloadImg.on("load", function() {
        img.attr("src", $(this).attr("src"));
    });
    downloadImg.attr("src", imgsrc);

    return it;
}

/**
 * Compare if exactly the elements of 'arr' are in the list 'list'
 * @param   list     List to check
 * @param   arr      Array to compare with
 * @retval  true     Identical
 * @retval  false    Not identical
 * */
function compareLists(list, arr)
{
    var len = list.length;
    if (len != arr.length)
    {
        return false;
    }

    for (var i=0; i < len; i++)
    {
        var d1 = $(list[i]).data("cardid");
        var d2 = $(arr[i]).data("cardid");
        if (d1 != d2)
        {
            return false;
        }
    }
    return true;
}

/**
 * Fill the element 'id' with the list
 * @param id    Element (<ul>)
 * @param list  Array of <li>
 * */
function setPlayout(id, list)
{
    var arr = [];
    list.forEach(function(element)
    {
        var it = createCardEntry(element.cardid);
        arr.push(it);
    });
    /* Before rewriting the DOM, check if it is already identical: */
    if (!compareLists($(id).children(), arr))
    {
        $(id).empty();
        $(id).append(arr);
    }
}

function setCardHeight(val)
{
    var px = val.toString() + "px";   
    $("body").get(0).style.setProperty("--card-height", px);   
}

function cmdSetCardHeight(val)
{
    $.ajax({
        method: "POST",
        data: { c: "setCardHeight",
                arg: val }
    })
    .done(function(json) {
        var data = jQuery.parseJSON(json);
        cardHeight = parseInt(data.cardHeight);
        setCardHeight(cardHeight);
    });
}

/**
 * This function will be executed periodically 
 * */
function periodic()
{
    if (!cardDeck)
    {
        return;
    }
    $.ajax({
        method: "POST",
        data: { c: "periodicInfo"  }
    })
    .done(function(json) {
        var data = jQuery.parseJSON(json);
        var openCards = data.openCards;

        openCards.forEach(function(obj)
        {
            var pos = obj.pos;
            setPlayout("#playout" + pos, obj.cards);
            $(".nameplayer" + pos + " .noCards").text(obj.noCards);
        });

        $(".playoutothers").removeClass("playoutnext");

        var next = data.info.nextplayerPos;
        if (next !== undefined)
        {
            $("#playout" + next).addClass("playoutnext");
        }

        var val = data.info.shuffleCount;
        if (val !== undefined)
        {
            if (val != shuffleCount)
            {
                shuffleCount = val;
                $("#getnewcards").click();
            }
        }
        
    });

}

function emptyAll()
{
    $("#cards").empty();
    $(".playoutothers").empty();
    $("#countResult").empty();
    $("#exchange").empty();
    $("#myplayout").empty();
}

/**
 * Playout all cards that are in the list.
 * The list is cleared.
 * @param list  List to playout
 * */
function playoutCards(list)
{
    var cardslist=[];
    $(list).children().each(function()
    {
        cardslist.push($(this).data('cardid'));
    });

    $(list).empty();
    $.ajax({
        method: "POST",
        data: { c: "playCards",
                cards: cardslist  }
    })
     .done(function(json) {
    });
}

$(document).ready( function() {

    $( "#cards" ).sortable({
        placeholder: "highlight",
        connectWith: "#exchange, #myplayout"
    });

    $( "#myplayout" ).sortable({
        placeholder: "highlight",
        connectWith: "#cards"
    });

    $( "#exchange" ).sortable({
        placeholder: "highlight",
        connectWith: "#cards"
    });

    $( "#cards" ).sortable({
        update: function(event, ui) {
            var sortlist=[];
            $("#cards li").each(function(i)
            {
                sortlist.push({ cardid: $(this).data("cardid"), order: i});
            });            

            $.ajax({
                method: "POST",
                data: { c: "reorder", order: sortlist  }
            })
             .done(function(json) {
            });
        }
    });

    $( "#myplayout" ).sortable({
        update: function(event, ui) {
            playoutCards("#myplayout");
        }
    });


    $("#cards, #myplayout, #exchange").disableSelection();

    $("#pushOnStack").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "pushOnStack"  }
        })
         .done(function(json) {
        });
    });

    $("#popFromStack").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "popFromStack"  }
        })
         .done(function(json) {
             $("#getnewcards").click();
        });
    });

    $("#getnewcards").click(function() {
        emptyAll();
        $.ajax({
            method: "POST",
            data: { c: "getCards"  }
        })
        .done(function(json) {
            var data = jQuery.parseJSON(json);
            cardDeck = data.cardDeck;
            data.cards.forEach(function(element)
            {
                var it = createCardEntry(element);
                $("#cards").append(it);
            });

            $("#playerSelect").empty();

            data.player.forEach(function(player)
            {
                var pos = player.pos;
                var name = player.name;
                $(".nameplayer" + pos + " .name").text(name);

                var optionAttr = { value: name, text: name };
                if (pos == 0)
                {
                    optionAttr.selected = 'selected';
                }
                $("#playerSelect").append($('<option>', optionAttr));

            });
        });
    });


    var shuffleDialog = $("#shuffle-dialog-form").dialog({
            autoOpen: false,
            modal: true,
            position: { of: $("#shuffle"),
                        my: "center top",
                        at: "center bottom"}
        });

    $("#shuffle").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "getShuffleOptions"  }
        })
        .done(function(json) {
            var shuffleOptions = jQuery.parseJSON(json);
            if ("use9" in shuffleOptions)
            {
                $("#shuffle-dialog-use9").prop("checked", shuffleOptions.use9);
            }
            shuffleDialog.dialog("open");
        });
    });

    $("#shuffle-dialog-submit").click(function() {
        shuffleDialog.dialog("close");
        emptyAll();
        var args = { use9 : $("#shuffle-dialog-use9").is(":checked") };
        $.ajax({
            method: "POST",
            data: { c: "shuffle", shuffleOptions: JSON.stringify(args)  }
        })
         .done(function(json) {
        });
    });

    var countDialog = $("#count-dialog").dialog({
            autoOpen: false,
            modal: true,
            position: { of: $("#count"), 
                        my: "center top",
                        at: "center bottom"}
        });

    $("#count").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "countCards"  }
        })
        .done(function(json) {
            var data = jQuery.parseJSON(json);
            $("#countResult").empty();
            if (data.error)
            {
                var li = $("<li><b>" + data.error + "</b></li>");
                $("#countResult").append(li);
            }
            else
            {
                data.result.forEach(function(element)
                {
                    var li = $("<li><b>" + element["name"] + "</b> : " + element["sum"] + "</li>");
                    $("#countResult").append(li);
                });
            }
            countDialog.dialog("open");
        });
    });

    $("#exchangeWrite").click(function() {
        var cardslist=[];
        $("#exchange").children().each(function()
        {
            cardslist.push($(this).data('cardid'));
        });
        $.ajax({
            method: "POST",
            data: { c: "exchangeWrite", cards: cardslist, exchangeId: 0  }
        })
         .done(function(json) {
         });
        $("#exchange").empty();
    });

    $("#exchangeRead").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "exchangeRead", exchangeId:0  }
        })
        .done(function(json) {
            var arr = jQuery.parseJSON(json);
            arr.forEach(function(element)
            {
                var it = createCardEntry(element);
                $("#exchange").append(it);
            });
        });
    });

    $("#unplayCard").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "unplayCards"  }
        })
        .done(function(json) {
            $("#getnewcards").click();
        });
    });

    $("#playAllCards").click(function() {
        playoutCards("#cards");
    });

    $("#playerSelect").change(function() {
        var str=$("#playerSelect option:selected").text();
        $.ajax({
            method: "POST",
            data: { c: "setPlayer", name: str }
        })
        .done(function(json) {
            $("#getnewcards").click();
        });
    });

    $("#logout").click(function() {
        clearInterval(intervalHandle);
        $.ajax({
            method: "POST",
            data: { c: "logout"  }
        })
        .done(function(json) {
            location.reload();
        });
    });

    $("#deleteDB").click(function() {
        $.ajax({
            method: "POST",
            data: { c: "deleteDB"  }
        })
        .done(function(json) {
            location.reload();
        });
    });

    $("#scaleUp").click(function() {
        cmdSetCardHeight(cardHeight+10);    
    });

    $("#scaleDown").click(function() {
        cmdSetCardHeight(cardHeight-10);    
    });

    $("button").addClass("ui-button ui-widget compact-button");




    $.ajax({
        method: "POST",
        data: { c: "init"  }
    })
    .done(function(json) {
        var data = jQuery.parseJSON(json);
        // Set globals:
        var admin = data.admin;
        cardHeight = parseInt(data.cardHeight);
        setCardHeight(cardHeight);
        if (admin)
        {
            $(".hidden").removeClass("hidden");
            $("#playerSelect").val(data.playerName);
        }
        $("#table").text(data.table);
        $("#playerName").text(data.playerName);
        $("#getnewcards").click();
    });

    intervalHandle = setInterval(periodic, 250);

} );
