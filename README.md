# Doppelkopf-Online

This is a PHP/Javascript based online implementation of the card game "Doppelkopf".
It allows four players to play together Doppelkopf using their web browsers.

## Prerequisites
You need to install the PHP files on a web server that is accessible by all players.

### PHP Requirements
* The development has been done using PHP 7.3.
* PHP sqlite support is required

### jQuery / jQuery-UI
For the html/javascript stuff, the two javascript packages jQuery (https://jquery.com) 
and jQuery-UI (https://jqueryui.com) are required.
Development has been done using
* jquery-ui version 1.12.1
* jquery version 1.12.4

## Installation
Please create your own version of *config.php* based on *config-template.php*.
Within this file you have to adapt the access codes and the number 
of tables you want to serve.

Install the PHP files into your webspace,
the card files (.svg) are in sub-folder *deck*. 

Download *jquery-ui* from https://jqueryui.com/
and place the jquery-ui.js (or a symlink) in the same directory as the other files. Also, the style sheet *jquery-ui.css* is required in this directory.

Download *jquery* from https://jquery.com/
and place the jquery.js (or a symlink) in the same directory as the other files.


The whole directory tree should look like:

    doppelkopf.php
    doppelkopf.html
    doppelkopf.css
    doppelkopf.js
    cardgame.php
    jquery-ui/jquery-ui.js    (jquery-ui is typically a symbolic link)
    jquery-ui/jquery-ui.css  
    jquery.js       (typically a symbolic link) 
    jquery.ui.touch-punch.js
    config.php
    deck/
    jquery-ui/
        external/
            jquery/

Within this directory the sqlite database files will be generated.
One database will be generated per served table.

The entry point to start the game is "doppelkopf.php".

You should adapt your *.htaccess* file to something like
```apache 
<FilesMatch "\.db$">
   <IfModule !mod_authz_core.c>
        Order allow,deny
        Deny from all
        Satisfy All
    </IfModule>
    <IfModule mod_authz_core.c>
        Require all denied
    </IfModule>
</FilesMatch>
```

to restrict the access to the database files.

## Cards
The cards in the deck have been taken from https://pixabay.com, 
some slight adaptions have been made on some of the cards.


## User Manual
The usage of the application is explained in German only...


## Spielanleitung
Hinweis: Um sinnvoll spielen zu können ist ein weiterer Kommunikationskanal 
nötig (Telefon, Chat, usw.)

## Anmelden
Zuerst müssen sich vier Spieler pro Tisch mit ihrem (eindeutigen) Namen anmelden.
Dazu müssen sie den Anmeldecode kennen (ACCESS_CODE in *config.php*).
Dann kann das Spiel losgehen.

## Spielaufbau

### Spielbeginn
Zu Spielbeginn klickt einer der Spieler auf "Karten mischen".
Danach klicken alle Spieler auf "Karten aufnehmen", damit sehen sie ihre Karten.

### Spielablauf
Der Screenshot zeigt den grundsätzlichen Aufbau:
![Screenshot 1](screenshot/doko-1.jpg "Screenshot 1")

Im unteren, hellgrünen Bereich sieht man die eigenen Karten.
Die Karten der Mitspieler sieht man oben in den drei Feldern, die Spielernamen 
sind darüber sichtbar.

Die eigenen Karten können sortiert werden.
Eine Karte wird ausgespielt, indem man sie von unten in den mittleren hellgrünen
Bereich zieht (links neben "Stich nehmen").
Von dort wird die Karte automatisch in den dunkelgrünen Bereich verschoben und
damit für die Mitspieler sichtbar.

Nachdem alle Spieler ihre Karte ausgespielt haben, so kann der Spieler mit der
höchsten Karte den Stich an sich nehmen, indem er auf den Button "Stich nehmen"
klickt.
Er ist dann der Startspieler für die nächste Runde. Das erkennt man daran, dass
der Ausspielbereich gelb markiert ist.

Achtung: Es gibt hier keinerlei Logik. D.h. ein "Vorwerfen" der Karten wird
nicht verhindert usw.

Nachdem alle Karten ausgespielt wurden, so kann man auf "Zählen" klicken.
Es werden dann die Punkte aller Spieler angezeigt.
Hinweis: Zählen ist erst dann möglich, wenn alle Karten gespielt wurden!

### Austauschbereich
Wenn es eine "Armut" gibt, so müssen Karten zwischen Spielern ausgetauscht
werden. Dazu dient der dunkelgraue Austauschbereich rechts unten.
Ein Spieler kann Karten von seiner Hand in den Austauschbereich ziehen.
Dann klickt er auf "In Austauschbereich geben". Der Empfänger der Karten kann
dann auf "Aus Austauschbereich nehmen" klicken und hat damit die Karten
in seinem Austauschbereich vorliegen.
Von dort kann er sie aufnehmen und auf gleiche Art und Weise wieder Karten 
zurücktransferieren.

### Sonderaktionen
Wenn man aus Versehen falsch ausgespielt hat, so kann man mit 
"Karte zurücknehmen" die Karte wieder auf die Hand nehmen.

Falls man versehentlich einen Stich aufgenommen hat, der eigentlich einem
Mitspieler zusteht, so kann man per "Stich zurücknehmen" den letzten Stich 
wieder in den offenen Bereich bringen.
Der richtige Spieler kann ihn dann per "Stich nehmen" übernehmen.

### Spielende
Zum Ende des Spiels sollten sich die Spieler per "Logout" wieder abmelden.

## Admin
Es gibt einen speziellen Login "admin", das zugehörige Passwort wird
in *config.php* bei *ADMIN_PASSWORD* festgelegt.
Der admin hat z.B. die Möglichkeit die Datenbank zu löschen.
Das ist zum Beispiel dann sinnvoll, wenn ein Spieler ausgestiegen ist ohne
sich abzumelden.
Damit kann man die Datenbank wieder in den Grundzustand bringen.
Allerdings müssen sich dann auch alle anderen Spieler wieder anmelden.










 
