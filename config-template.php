<?php

# Please adapt this code.
# It is the access code to your playground.
# Only the players should know it
$ACCESS_CODE = "doko";


# Please adapt this code
# This is the admin access.
# It is required e.g. to delete databases etc.
$ADMIN_PASSWORD = "admin";


# Number of served tables per game.
# There will be a separated sqlite database file for
# each table.
$NUMBER_TABLES = 2;

?>
