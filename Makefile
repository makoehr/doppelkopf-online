### This Makefile is used for development purposes only
### However, you might see the idea of the installation

INSTDIR:=$(DESTDIR)/var/www/html/doppelkopf

SCRIPT := cardgame.php doppelkopf.php
CONFIG := config.php
HTML :=   doppelkopf.html
JS   :=   doppelkopf.js
CSS  :=   doppelkopf.css

all:

config.php: config-template.php
	cp $< $@

install: $(CONFIG)
	install $(SCRIPT) $(INSTDIR)
	install -m 644 $(CONFIG) $(HTML) $(JS) $(CSS) $(INSTDIR)
	install -d $(INSTDIR)/deck
	install -m 644 deck/*.svg $(INSTDIR)/deck


